#!/usr/bin/env python
import glob
import grp
import os
import shutil
import sys

destination = "./archive"
if len(sys.argv) < 2:
    sys.exit("no group name given as an option to move files")
groupname = sys.argv[1]

grouptosearchfor = grp.getgrnam(groupname)
files = glob.glob('*', recursive=True)
groupfiles = []
for file in files:
    stat_of_file = os.stat(file)
    if grouptosearchfor.gr_gid == stat_of_file.st_gid: 
        groupfiles.append(file)

print(groupfiles)

print("moving files of ", groupname, "to archive folder")

for file in groupfiles:
    shutil.move(file, destination)
