Document: grouparchiver
Title: Debian grouparchiver Manual
Author: <insert document author here>
Abstract: This manual describes what grouparchiver is
 and how it can be used to
 manage online manuals on Debian systems.
Section: unknown

Format: debiandoc-sgml
Files: /usr/share/doc/grouparchiver/grouparchiver.sgml.gz

Format: postscript
Files: /usr/share/doc/grouparchiver/grouparchiver.ps.gz

Format: text
Files: /usr/share/doc/grouparchiver/grouparchiver.text.gz

Format: HTML
Index: /usr/share/doc/grouparchiver/html/index.html
Files: /usr/share/doc/grouparchiver/html/*.html
